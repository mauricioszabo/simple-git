## NEXT

## 0.3.3
* Fix refresh repos

## 0.3.0
* Modernizing codebase
* Toggle blame scrolls (kinda) the editor

## 0.1.2 - editor branch status fix
* Refresh editor's current branch when changing branches

## 0.1.1 - Simple fix
* Refresh editor GIT status after each commit

## 0.1.0 - First Release
* Simple git commands added: commit, commit current file, push, update master, new branch from current
* Added diff view
* Toggle blame
